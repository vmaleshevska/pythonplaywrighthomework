import locators as loc
from page_objects.base_page import BasePage
from playwright.sync_api import expect


class BrokerPage(BasePage):

    URL = "broker"

    def load_more_brokers(self):
        load_button = self.page.locator(loc.BrokerPageLocators.LOAD_MORE_BUTTON)
        load_button.click()
        expect(load_button).to_be_hidden()

    def get_broker_names(self) -> list[str]:
        self.wait_for_loading()
        return self.page.locator(loc.BrokerPageLocators.BROKER_CARD_NAME).all_inner_texts()

    def search_broker(self, name: str):
        with self.page.expect_request(f"{self.base_url}{self.URL}/filter*"):
            self.page.locator(loc.BrokerPageLocators.FILTER_SEARCH_INPUT).fill(name)
        self.wait_for_loading()

    def wait_for_loading(self):
        brokers_loading = self.page.locator(loc.BrokerPageLocators.BROKERS_LOADING)
        expect(brokers_loading).to_be_hidden()
