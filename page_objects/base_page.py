from playwright.sync_api import sync_playwright


class BasePage:
    URL: str

    def __init__(self) -> None:
        self.base_url = "https://www.yavlena.com/"
        playwright = sync_playwright().start()
        browser = playwright.chromium.launch(headless=False)
        self.page = browser.new_page()

    def navigate(self) -> None:
        self.page.goto(self.base_url + self.URL)
        self.page.wait_for_load_state(state="load")
