from playwright.sync_api import expect
from page_objects.broker_page import BrokerPage
import locators as loc


class TestBrokers:

    def test_validate_brokers(self):
        broker_page = BrokerPage()
        broker_page.navigate()
        broker_page.load_more_brokers()

        broker_names = broker_page.get_broker_names()
        for broker in broker_names:
            broker_page.search_broker(broker)
            displayed_broker_names = broker_page.get_broker_names()

            assert len(displayed_broker_names) == 1, "Incorrect amount of brokers displayed"
            assert displayed_broker_names[0] == broker, "Incorrect broker name displayed on page"

            expect(broker_page.page.locator(loc.BrokerPageLocators.BROKER_CARD_ADDRESS)).to_be_visible()
            expect(broker_page.page.locator(loc.BrokerPageLocators.BROKER_CARD_PROPERTIES)).to_be_visible()
            expect(broker_page.page.locator(loc.BrokerPageLocators.BROKER_CARD_PHONE)).not_to_have_count(0)

