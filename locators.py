class BrokerPageLocators:
    LOAD_MORE_BUTTON = "div.load-more-brokers > a"
    BROKERS_LOADING = "div.brokers-loading"
    FILTER_SEARCH_INPUT = "div.filter-bar input#searchBox"
    BROKER_CARD = ".broker-card"
    BROKER_CARD_NAME = f"{BROKER_CARD} h3.name > a"
    BROKER_CARD_ADDRESS = f"{BROKER_CARD} div.office"
    BROKER_CARD_PROPERTIES = f"{BROKER_CARD} div.position > a"
    BROKER_CARD_PHONE = f"{BROKER_CARD} span.tel > a"
